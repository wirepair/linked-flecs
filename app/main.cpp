#include <flecs.h>
#include "ecs/movement.h"

int main(int argc, char **argv)
{
    ecs_os_set_api_defaults();
    movement::init();

    flecs::world ecs {};
    //movement::module Mod(ecs);

    ecs.import<movement::module>();

    // Create entity with imported components
    flecs::entity ent = ecs.entity()
        .set<movement::Position>({10, 20, 1})
        .set<movement::Velocity>({1, 1, 1});

    ent.get([](const movement::Position& p) {
        printf("%f %f %f\n", p.x, p.y, p.z);
    });

    ecs.progress(); // tick

    ent.get([](const movement::Position& p) {
        printf("%f %f %f\n", p.x, p.y, p.z);
    });
}