# linked-flecs

Demonstrates dynamic linking fails, but static linking succeeds.

### Running
- `build/test/Debug/testlib.exe`


### Building Success/Failure:
Comment out the below lines for testing success/failure:

Success:
- https://gitlab.com/wirepair/linked-flecs/-/blob/68054ecf5d064dcc7f3070d3809f392844c63ff4/tests/CMakeLists.txt#L19

Failure:
- https://gitlab.com/wirepair/linked-flecs/-/blob/68054ecf5d064dcc7f3070d3809f392844c63ff4/tests/CMakeLists.txt#L22