#define CATCH_CONFIG_MAIN
#include <flecs.h>
#include <catch2/catch.hpp>
#include "../src/ecs/movement.h"

TEST_CASE( "Test ECS crashes when calling import when dynamically linked", "[main]" ) {
    flecs::world ecs {};
    
    ecs.import<movement::module>(); // <-- crashes

    // Create entity with imported components
    flecs::entity ent = ecs.entity()
        .set<movement::Position>({10, 20, 1})
        .set<movement::Velocity>({1, 1, 1});

    ent.get([](const movement::Position& p) {
        REQUIRE( p.x == 10 );
        REQUIRE( p.y == 20 );
        REQUIRE( p.z == 1 );
    });

    ecs.progress(); // tick

    ent.get([](const movement::Position& p) {
        REQUIRE( p.x == 11 );
        REQUIRE( p.y == 21 );
        REQUIRE( p.z == 2 );        
    });
}

TEST_CASE( "Test ECS calling movement::module Mod(world) first then import doesn't crash", "[main]" ) {
    flecs::world ecs {};
    
    movement::module Mod(ecs);

    ecs.import<movement::module>();

    // Create entity with imported components
    flecs::entity ent = ecs.entity()
        .set<movement::Position>({10, 20, 1})
        .set<movement::Velocity>({1, 1, 1});

    ent.get([](const movement::Position& p) {
        REQUIRE( p.x == 10 );
        REQUIRE( p.y == 20 );
        REQUIRE( p.z == 1 );
    });

    ecs.progress(); // tick

    ent.get([](const movement::Position& p) {
        REQUIRE( p.x == 11 );
        REQUIRE( p.y == 21 );
        REQUIRE( p.z == 2 );        
    });
}

TEST_CASE( "Test ECS calling new movement::module Mod(world) first then import doesn't crash", "[main]" ) {
    flecs::world ecs {};
    
    auto mod = new movement::module(ecs);

    ecs.import<movement::module>();

    // Create entity with imported components
    flecs::entity ent = ecs.entity()
        .set<movement::Position>({10, 20, 1})
        .set<movement::Velocity>({1, 1, 1});

    ent.get([](const movement::Position& p) {
        REQUIRE( p.x == 10 );
        REQUIRE( p.y == 20 );
        REQUIRE( p.z == 1 );
    });

    ecs.progress(); // tick

    ent.get([](const movement::Position& p) {
        REQUIRE( p.x == 11 );
        REQUIRE( p.y == 21 );
        REQUIRE( p.z == 2 );        
    });

    delete mod;
}