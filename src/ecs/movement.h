#pragma once

#include <flecs.h>

namespace movement 
{
    struct Position {
        float x;
        float y;
        float z; 
    };

    struct Velocity {
        float x;
        float y;
        float z;
    };

    struct Quat {
        float x;
        float y;
        float z;
        float w;
    };

    struct Rotation {
        float x;
        float y;
        float z;
    };

    struct module {
        module(flecs::world& world); // Ctor that loads the module
    };
}
